function love.conf(t)
    t.identity = "lovedoom"
    t.version = "0.10.2"
    t.window.title = "LÖVEDOOM"
    t.window.width = 320
    t.window.height = 200
    t.window.minwidth = 320
    t.window.minheight = 200
end
